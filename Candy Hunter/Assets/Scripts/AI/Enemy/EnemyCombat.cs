﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCombat : MonoBehaviour
{
    [SerializeField] private int m_Health;
    [SerializeField] private float m_DelayToDie;
    [SerializeField] private Slider m_Slider;

    void Start()
    {
        SetMaxHealth(m_Health);
    }

    void Update()
    {
        if (m_Health <= 0.0f)
        {
            Die();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            TakeDamage(other.gameObject.GetComponent<Projectile>().Damage);
            SetHealth(m_Health);
        }
    }

    void TakeDamage(int amount)
    {
        m_Health -= amount;
    }

    void SetMaxHealth(int health)
    {
        m_Slider.maxValue = health;
        m_Slider.value = health;
    }

    void SetHealth(int health)
    {
        m_Slider.value = health;
    }

    void Die()
    {
        Destroy(gameObject, m_DelayToDie);
    }
}
