﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float m_LookRadius;
    [SerializeField] private float m_SmoothRotSpeed;

    private Transform m_Target;
    private NavMeshAgent m_Agent;

    private float m_DistanceToPlayer;
    public float DistanceToPlayer { get => m_DistanceToPlayer; }

    void Start()
    {
        m_Target = PlayerManager.Instance.Player.transform;
        m_Agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        m_DistanceToPlayer = Vector3.Distance(m_Target.position, transform.position);

        if (m_DistanceToPlayer <= m_LookRadius)
        {
            m_Agent.SetDestination(m_Target.position);

            if (m_DistanceToPlayer <= m_Agent.stoppingDistance)
            {
                FaceTarget();
            }
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (m_Target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0.0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * m_SmoothRotSpeed);
    }
}