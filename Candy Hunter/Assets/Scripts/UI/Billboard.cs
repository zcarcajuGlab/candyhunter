﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Transform m_CameraPosition;
    public Transform CameraPosition { get => m_CameraPosition; set => m_CameraPosition = value; }
 
    void Update()
    {
        transform.LookAt(transform.position + m_CameraPosition.forward);
    }
}
