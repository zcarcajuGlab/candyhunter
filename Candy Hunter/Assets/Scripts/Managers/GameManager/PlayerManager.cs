﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    private GameObject m_Player;
    public GameObject Player { get => m_Player; }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        m_Player = GameObject.FindGameObjectWithTag("Player");
        Instance = this;
    }
}