﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private float m_CheckRate;
    [SerializeField] private float m_CheckClosestEnemyRate;

    private GameObject[] m_EnemiesInScene;
    public GameObject[] EnemiesInScene { get => m_EnemiesInScene; }

    private Billboard[] m_BillboardObjects;

    private Transform m_CameraPosition;

    private PlayerCombat m_Player;

    bool m_CanCheckClosest = true;

    void Awake()
    {
        m_Player = FindObjectOfType<PlayerCombat>();
        m_Player.LevelManager = this;

        m_EnemiesInScene = GameObject.FindGameObjectsWithTag("Enemy");
        m_Player.ClosestEnemy = GetClosestEnemyToPlayer();
        StartCoroutine(CheckNumOfEnemies());
        StartCoroutine(CheckClosestEnemy());

        m_CameraPosition = GameObject.Find("Main Camera").transform;
        m_BillboardObjects = FindObjectsOfType<Billboard>();
        SetBillboards();
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (m_EnemiesInScene.Length == 0)
        {
            m_CanCheckClosest = false;
        }
    }

    void SetBillboards()
    {
        for (int i = 0; i < m_BillboardObjects.Length; ++i)
        {
            m_BillboardObjects[i].GetComponent<Billboard>().CameraPosition = m_CameraPosition;
        }
    }

    EnemyController GetClosestEnemyToPlayer()
    {
        float closestDistance = 30.0f;
        EnemyController closestEnemy = null;

        for (int i = 0; i < m_EnemiesInScene.Length; ++i)
        {
            if (m_EnemiesInScene.Length != 0)
            {
                EnemyController temp = m_EnemiesInScene[i].GetComponent<EnemyController>();
                if (temp.DistanceToPlayer < closestDistance)
                {
                    closestDistance = temp.DistanceToPlayer;
                    closestEnemy = temp;
                }
            }
        }

        return closestEnemy;
    }

    IEnumerator CheckNumOfEnemies()
    {
        yield return new WaitForSeconds(m_CheckRate);
        m_EnemiesInScene = GameObject.FindGameObjectsWithTag("Enemy");

        StartCoroutine(CheckNumOfEnemies());
    }

    IEnumerator CheckClosestEnemy()
    {
        yield return new WaitForSeconds(m_CheckClosestEnemyRate);

        if (m_EnemiesInScene.Length != 0 && m_CanCheckClosest)
        {
            StartCoroutine(CheckClosestEnemy());
        }
        else
        {
            m_Player.ClosestEnemy = null;
        }

        m_Player.ClosestEnemy = GetClosestEnemyToPlayer();
    }
}