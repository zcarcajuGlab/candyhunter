﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCombat : MonoBehaviour
{
    private LevelManager m_LevelManager;
    public LevelManager LevelManager { get => m_LevelManager; set => m_LevelManager = value; }

    private EnemyController m_ClosestEnemy;
    public EnemyController ClosestEnemy { get => m_ClosestEnemy; set => m_ClosestEnemy = value; }

    private PlayerMovement m_PlayerMovement;

    [Header("Shooting")]
    [SerializeField] private Transform m_ProjectileOrigin;
    [SerializeField] private float m_ShootRate;

    [Header("Hit Points")]
    [SerializeField] private Slider m_Slider;
    [SerializeField] private int m_Health;

    private float m_ShootController;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        m_PlayerMovement = GetComponent<PlayerMovement>();
        SetMaxHealth(m_Health);
    }

    void Update()
    {
        if (m_ClosestEnemy != null)
        {
            if (m_PlayerMovement.IsIdle)
            {
                transform.LookAt(m_ClosestEnemy.gameObject.transform);
                m_ShootController += m_ShootRate * Time.deltaTime;

                if (m_ShootController >= m_ShootRate)
                {
                    Shoot();
                    m_ShootController = 0.0f;
                }
            }
            else
            {
                m_ShootController = 0.0f;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //TakeDamage(other.gameObject.GetComponent<EnemyCombat>().Damage);
            //SetHealth(m_Health);
        }
    }

    void Shoot()
    {
        GameObject projectile = ObjectPool.SharedInstance.GetPooledObject("Projectile");
        if (projectile != null)
        {
            Vector3 origin = m_ProjectileOrigin.position;
            projectile.transform.position = new Vector3(origin.x, origin.y, origin.z);
            projectile.transform.rotation = gameObject.transform.rotation;
            projectile.SetActive(true);
        }
    }

    void TakeDamage(int amount)
    {
        m_Health -= amount;
    }

    void SetMaxHealth(int health)
    {
        m_Slider.maxValue = health;
        m_Slider.value = health;
    }

    void SetHealth(int health)
    {
        m_Slider.value = health;
    }
}