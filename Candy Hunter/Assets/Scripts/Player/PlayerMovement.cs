﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private int m_RewiredIndex;
    [SerializeField] private float m_Speed;

    private float m_TempSpeed;

    private bool m_IsIdle;
    public bool IsIdle { get => m_IsIdle; }

    private Rewired.Player m_RePlayer;
    private Vector3 m_InputValues;
    public Vector3 InputValues { get => m_InputValues; }
    private Vector3 m_FacingRotation;
    public Vector3 FacingRotation { get => m_FacingRotation; set => m_FacingRotation = value; }

    private Rigidbody m_Rb;

    void Start()
    {
        m_TempSpeed = m_Speed;
        m_RePlayer = ReInput.players.GetPlayer(m_RewiredIndex);
        m_InputValues = Vector3.zero;
        m_Rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (m_InputValues != Vector3.zero)
        {
            m_IsIdle = false;
            m_Speed = m_TempSpeed;
        }
        else
        {
            m_IsIdle = true;
        }

        m_FacingRotation = Vector3.Normalize(m_InputValues);
        if (m_FacingRotation != Vector3.zero)
        {
            transform.forward = m_FacingRotation;
        }

        //Moving with the inputs
        m_InputValues.x = m_RePlayer.GetAxis("Movement Horizontal") * m_Speed * Time.deltaTime;
        m_InputValues.z = m_RePlayer.GetAxis("Movement Vertical") * m_Speed * Time.deltaTime;

        if (Mathf.Abs(m_InputValues.x) > Mathf.Epsilon && Mathf.Abs(m_InputValues.z) > Mathf.Epsilon)
        {
            m_Speed *= 0.7071f;
        }
    }

    void FixedUpdate()
    {
        m_Rb.MovePosition(transform.position + m_InputValues);   
    }
}