﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float m_Speed;
    [SerializeField] private float m_DisableTimeIfNoHit;
    [SerializeField] private int m_Damage;
    public int Damage { get => m_Damage; }

    protected virtual void OnEnable()
    {

    }

    protected virtual void Update()
    {
        transform.position += transform.forward * m_Speed * Time.deltaTime;
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            gameObject.SetActive(false);
        }
    }

    protected virtual void OnDisable()
    {

    }

    protected IEnumerator DisableIfNoHit()
    {
        yield return new WaitForSeconds(m_DisableTimeIfNoHit);
        gameObject.SetActive(false);
    }
}
